// SPDX-License-Identifier: Apache-2.0
#define PY_SSIZE_T_CLEAN
#include <Python.h>
#define PY_ARRAY_UNIQUE_SYMBOL jtag_kernel_ARRAY_API
#include <numpy/arrayobject.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include "jtag.hpp"

/* Global variables */
Jtag GLOBAL_JTAG;

const char* CRED = "\33[31m";
const char* CEND = "\33[0m";

void print_cpp_function(const char* name) {
    printf("[%sC++%s] jtag_kernel: %s\n", CRED, CEND, name);
}
void not_implemented(const char* caller) {
    printf("%s[%s] FIXME: Not implemented!%s\n", CRED, caller, CEND);
    exit(-1);
}


/** Functions exposed to python3 **/

static PyObject* set_programmer_config(PyObject* self, PyObject* args) {
    print_cpp_function(__FUNCTION__);
    
    PyArrayObject* prog_data = NULL;
    struct jtag_programmer_config config;

    // Parse arguments
    if (!PyArg_ParseTuple(args, "iiiiiiii",
            &config.len_ir,
            &config.ir_addr,
            &config.ir_data,
            &config.ir_wen,
            &config.len_addr,
            &config.len_data,
            &config.len_wen,
            &config.trstn_polarity
    )) {
        return NULL;
    }
    print_config(config);

    GLOBAL_JTAG.set_programmer_config(config);

    return Py_None;
}

static PyObject* gen_prog_waveform(PyObject* self, PyObject* args) {
    print_cpp_function(__FUNCTION__);
    
    PyArrayObject* prog_data = NULL;
    struct jtag_programmer_config config;
    int trstn_polarity;

    // Parse arguments
    if (!PyArg_ParseTuple(args, "O!",
            &PyArray_Type, &prog_data
    )) {
        return NULL;
    }

    /* validate input array dimensions */
    if (PyArray_NDIM(prog_data) != 2) {
        PyErr_SetString(PyExc_ValueError, "`prog_data` must be two dimensional");
        return NULL;
    }
    if (PyArray_DIM(prog_data, 1) != 2) {
        PyErr_SetString(PyExc_ValueError, "`prog_data.shape[1]` must be equal to 2");
        return NULL;
    }
    if (PyArray_TYPE(prog_data) != NPY_UINT) {
        PyErr_SetString(PyExc_ValueError, "`prog_data` must have type `np.uint32`");
        return NULL;
    }

    GLOBAL_JTAG.gen_prog_waveform(prog_data);

    return Py_None;
}

static PyObject* init(PyObject* self, PyObject* args) {
    print_cpp_function(__FUNCTION__);
    return Py_None;
}

static PyObject* reset_tap(PyObject* self, PyObject* args) {
    GLOBAL_JTAG.reset_tap();
    return Py_None;
}

static PyObject* cycle_tms(PyObject* self, PyObject* args) {
    print_cpp_function(__FUNCTION__);
    int tms;
    if (!PyArg_ParseTuple(args, "i", &tms)) {
        return NULL;
    }
    GLOBAL_JTAG.jtag_cycle_tms(tms);
    return Py_None;
}

static PyObject* clear(PyObject* self, PyObject* args) {
    print_cpp_function(__FUNCTION__);
    GLOBAL_JTAG.clear();
    return Py_None;
}

static PyObject* cycle_tms_tdi(PyObject* self, PyObject* args) {
    print_cpp_function(__FUNCTION__);
    int tms, tdi;
    if (!PyArg_ParseTuple(args, "ii", &tms, &tdi)) {
        return NULL;
    }
    GLOBAL_JTAG.jtag_cycle_tms_tdi(tms, tdi);
    return Py_None;
}

static PyObject* get_remote_bitbang(PyObject* self, PyObject* args) {
    print_cpp_function(__FUNCTION__);
    std::string remote_bitbang_str = GLOBAL_JTAG.get_remote_bitbang();
    PyObject* obj = Py_BuildValue("y#", remote_bitbang_str.c_str(), remote_bitbang_str.size());
    return obj;
}

static PyObject* get_numpy_array(PyObject* self, PyObject* args) {
    print_cpp_function(__FUNCTION__);
    return GLOBAL_JTAG.get_numpy_array();
}


// flags documentation: https://docs.python.org/3/c-api/structures.html#c.PyMethodDef
static PyMethodDef my_methods[] = {
    {"init", init, METH_VARARGS, "Initialize accelerator kernel."},
    {"clear", clear, METH_VARARGS, "Clear jtag waveform."},
    {"cycle_tms", cycle_tms, METH_VARARGS, "Cycle TCK once while holding TMS at given value."},
    {"cycle_tms_tdi", cycle_tms_tdi, METH_VARARGS, "Cycle TCK once while holding TMS and TDI at given values."},
    {"reset_tap", reset_tap, METH_VARARGS, "Cycle trst reset once."},
    {"set_programmer_config", set_programmer_config, METH_VARARGS, "Set JTAG programmer configuration."},
    {"gen_prog_waveform", gen_prog_waveform, METH_VARARGS, "Sample text."},
    {"get_remote_bitbang", get_remote_bitbang, METH_VARARGS, "Get JTAG waveform in remote bitbang encoding."},
    {"get_numpy_array", get_numpy_array, METH_VARARGS, "Get JTAG waveform as a numpy array."},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef jtag_kernel = {
    PyModuleDef_HEAD_INIT,
    "jtag_kernel",
    "Compute kernel for accelerating A-Core JTAG programming waveform generation.",
    -1,
    my_methods,
};

PyMODINIT_FUNC PyInit_jtag_kernel(void) {
    import_array();
    return PyModule_Create(&jtag_kernel);
}
