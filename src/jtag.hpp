#pragma once
#define PY_SSIZE_T_CLEAN
#include <Python.h>
#define NO_IMPORT_ARRAY
#define PY_ARRAY_UNIQUE_SYMBOL jtag_kernel_ARRAY_API
#include <numpy/arrayobject.h>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <vector>
#include <string>

std::ostream& operator<< (std::ostream& os, const std::vector<char> v);


struct jtag_programmer_config {
    // Instruction Register
    int len_ir;
    // Test Data Registers
    int ir_addr;
    int ir_data;
    int ir_wen;
    int len_addr;
    int len_data;
    int len_wen;
    // Resets
    int trstn_polarity;
};
void print_config(struct jtag_programmer_config config);

struct signals {
    uint8_t tck;
    uint8_t tms;
    uint8_t tdi;
};

class Jtag {
public:
    Jtag ();

    /** Accessors **/
    void set_programmer_config(struct jtag_programmer_config config);
    std::string get_remote_bitbang();
    PyObject* get_numpy_array();

    /** JTAG primitive helper routines ***/
    void jtag_cycle_tms(int tms);
    void jtag_cycle_tms_tdi(int tms, int tdi);
    void idle_to_drshift();
    void idle_to_irshift();
    void drshift_to_idle();
    void shift(int tdi, size_t len_tdi);
    void irshift_to_idle();
    void shift_instruction(int ir, size_t len_ir);
    void shift_data(int tdi, size_t len_tdi);
    void shift_to_chain(int ir, size_t len_ir, int tdi, size_t len_tdi);
    void reset_tap();
    void cycle_tms();
    void cycle_tms_tdi();
    void clear();

    /** Higher-level helpers **/
    bool gen_prog_waveform(PyArrayObject* prog_data);

    /** Miscellaneous helpers **/
    static unsigned int count_jtag_steps(int n_bytes, struct jtag_programmer_config config);
    void print();

private:
    static inline char map_jtag_ifelse(uint8_t tck, uint8_t tms, uint8_t tdi);
    static inline char map_jtag_lookup(uint8_t tck, uint8_t tms, uint8_t tdi);
    static struct signals rb_to_signals(char rb_symbol);

    struct jtag_programmer_config m_programmer_config;
    std::vector<char> m_waveform;
    uint8_t m_old_tdi;
};
