#include "jtag.hpp"

std::ostream& operator<< (std::ostream& os, const std::vector<char> v) {
    for (auto c : v)
        os << c;
    return os;
}

void print_config(struct jtag_programmer_config config) {
    printf("struct jtag_programmer_config {\n");
    printf("  len_ir = %d,\n", config.len_ir);
    printf("  ir_addr = %d,\n", config.ir_addr);
    printf("  ir_data = %d,\n", config.ir_data);
    printf("  ir_wen = %d,\n", config.ir_wen);
    printf("  len_addr = %d,\n", config.len_addr);
    printf("  len_data = %d,\n", config.len_data);
    printf("  len_wen = %d,\n", config.len_wen);
    printf("  trstn_polarity = %d\n", config.trstn_polarity);
    printf("}\n");
}

Jtag::Jtag() {
    m_waveform = std::vector<char>();
}

void Jtag::clear() {
    m_waveform = std::vector<char>();
}

void Jtag::set_programmer_config(struct jtag_programmer_config config) {
    m_programmer_config = config;
}

// TODO: does this blow up if
//  - m_waveform is uninitialized
//  - m_waveform is empty (size = 0)
std::string Jtag::get_remote_bitbang() {
    std::string remote_bitbang_str(this->m_waveform.data(), this->m_waveform.size());
    return remote_bitbang_str;
}

PyObject* Jtag::get_numpy_array() {
    /* Pre-allocate new numpy array */
    npy_intp dims[2] = {-1, 4};
    dims[0] = this->m_waveform.size();
    std::cout << "dims: " << dims[0] << " " << dims[1] << std::endl;
    PyObject* arr = PyArray_SimpleNew(2, dims, NPY_UBYTE);
    if (arr == NULL) {
        PyErr_SetString(PyExc_RuntimeError, "could not allocate memory for np array");
        return NULL;
    }
    for (int i = 0; i < dims[0]; i++) {
        char bitbang_symbol = this->m_waveform[i];
        struct signals sample = Jtag::rb_to_signals(bitbang_symbol);
        if (sample.tck == 255 || sample.tms == 255 || sample.tdi == 255) {
            printf("invalid character in remote bitbang waveform: %c (0x%02x)\n", bitbang_symbol, bitbang_symbol);
            // PyErr_SetString(PyExc_RuntimeError, "invalid character in remote bitbang waveform");
            // return NULL;
        }
        *((uint8_t*)PyArray_GETPTR2(arr, i, 0)) = sample.tck; // TCK
        *((uint8_t*)PyArray_GETPTR2(arr, i, 1)) = sample.tms; // TMS
        *((uint8_t*)PyArray_GETPTR2(arr, i, 2)) = sample.tdi; // TDI
        // TODO: handle tap reset signal
        *((uint8_t*)PyArray_GETPTR2(arr, i, 3)) = !this->m_programmer_config.trstn_polarity; // TRSTn
    }

    return arr;
}

/** JTAG primitive helper routines ***/

void Jtag::jtag_cycle_tms(int tms) {
    this->m_waveform.push_back(map_jtag_ifelse(0, tms, this->m_old_tdi));
    this->m_waveform.push_back(map_jtag_ifelse(1, tms, this->m_old_tdi));
}

void Jtag::jtag_cycle_tms_tdi(int tms, int tdi) {
    this->m_old_tdi = tdi;
    jtag_cycle_tms(tms);
}

void Jtag::idle_to_drshift(void) {
    jtag_cycle_tms(1);   // state 12 = Run-Test/Idle
    jtag_cycle_tms(0);   // state 7 = Select-DR-Scan
    jtag_cycle_tms(0);   // state 6 = Capture-DR
}

void Jtag::idle_to_irshift(void) {
    jtag_cycle_tms(1);   // state 12 = Run-Test/Idle
    jtag_cycle_tms(1);   // state 7 = Select-DR-Scan
    jtag_cycle_tms(0);   // state 4 = Select-IR-Scan
    jtag_cycle_tms(0);   // state 14 = Capture-IR
}

void Jtag::drshift_to_idle(void) {
    jtag_cycle_tms(1);   // state 1 = Exit1-DR
    jtag_cycle_tms(0);   // state 5 = Update-DR
}

void Jtag::shift(int tdi, size_t len_tdi) {
    for (size_t i = 0; i < len_tdi-1; i++) {
        jtag_cycle_tms_tdi(0, tdi & 1);
        tdi >>= 1;
    }
    jtag_cycle_tms_tdi(1, tdi & 1);
}

void Jtag::irshift_to_idle(void) {
    jtag_cycle_tms(1);   // state 9 = Exit1-IR
    jtag_cycle_tms(0);   // state 13 = Update-IR
}

void Jtag::shift_instruction(int ir, size_t len_ir) {
    idle_to_irshift();   // 4 cycles
    shift(ir, len_ir);   // `len_tdi` cycles
    irshift_to_idle();   // 2 cycles
}

void Jtag::shift_data(int tdi, size_t len_tdi) {
    idle_to_drshift();   // 3 cycles
    shift(tdi, len_tdi); // `len_tdi` cycles
    drshift_to_idle();   // 2 cycles
}

void Jtag::shift_to_chain(int ir, size_t len_ir, int tdi, size_t len_tdi) {
    shift_instruction(ir, len_ir);  // `len_ir` + 6 cycles
    shift_data(tdi, len_tdi);       // `len_tdi` + 5 cycles
}

void Jtag::reset_tap() {
    // TODO: check how OpenOcd handles trst and srst polarities
    //       current implementation assumes active low reset for both
    this->m_waveform.push_back('s');
    this->m_waveform.push_back('u');
}


/** Higher-level helpers **/
bool Jtag::gen_prog_waveform(PyArrayObject* prog_data) {

    // deassert TRSTn based on polarity
    //TRSTn = (trstn_polarity == 1) ? 0 : 1;

    /* validate input array dimensions */
    if (PyArray_NDIM(prog_data) != 2) {
        PyErr_SetString(PyExc_ValueError, "`prog_data` must be two dimensional");
        return false;
    }
    if (PyArray_DIM(prog_data, 1) != 2) {
        PyErr_SetString(PyExc_ValueError, "`prog_data.shape[1]` must be equal to 2");
        return false;
    }
    if (PyArray_TYPE(prog_data) != NPY_UINT) {
        PyErr_SetString(PyExc_ValueError, "`prog_data` must have type `np.uint32`");
        return false;
    }

    // TODO: pre-allocate size for Jtag remote bitbang vector
    int n_bytes = PyArray_DIM(prog_data,0);
    int prealloc_size = count_jtag_steps(n_bytes, this->m_programmer_config);

    // shift programming data
    // TODO: In case of performance issues
    //       * Unroll this loop
    //       * Use `#pragma omp parallel for` on the outermost loop
    for (int i = 0; i < n_bytes; i++) {
        uint32_t addr = *((uint32_t*)PyArray_GETPTR2(prog_data, i, 0));
        uint32_t data = *((uint32_t*)PyArray_GETPTR2(prog_data, i, 1));

        // shift in address and data
        this->shift_to_chain(this->m_programmer_config.ir_addr, this->m_programmer_config.len_ir, addr, this->m_programmer_config.len_addr);
        this->shift_to_chain(this->m_programmer_config.ir_data, this->m_programmer_config.len_ir, data, this->m_programmer_config.len_data);

        // strobe write_en
        this->shift_to_chain(this->m_programmer_config.ir_wen, this->m_programmer_config.len_ir, 1, this->m_programmer_config.len_wen);
        this->shift_to_chain(this->m_programmer_config.ir_wen, this->m_programmer_config.len_ir, 0, this->m_programmer_config.len_wen);
    }

    return true;
}


/**
 * Mapping functions from (tck, tms, tdi) to remote bitbang char
 * TODO: Profile 'map_jtag_ifelse' and 'map_jtag_lookup' and use the faster one
 */

// if-else map
inline char Jtag::map_jtag_ifelse(uint8_t tck, uint8_t tms, uint8_t tdi) {
    assert (tck == 0 || tck == 1);
    assert (tms == 0 || tms == 1);
    assert (tdi == 0 || tdi == 1);
    if      (tck == 0 && tms == 0 && tdi == 0) return '0';
    else if (tck == 0 && tms == 0 && tdi == 1) return '1';
    else if (tck == 0 && tms == 1 && tdi == 0) return '2';
    else if (tck == 0 && tms == 1 && tdi == 1) return '3';
    else if (tck == 1 && tms == 0 && tdi == 0) return '4';
    else if (tck == 1 && tms == 0 && tdi == 1) return '5';
    else if (tck == 1 && tms == 1 && tdi == 0) return '6';
    else                                       return '7';
}

// lookup
inline char Jtag::map_jtag_lookup(uint8_t tck, uint8_t tms, uint8_t tdi) {
    assert (tck == 0 || tck == 1);
    assert (tms == 0 || tms == 1);
    assert (tdi == 0 || tdi == 1);
    char lookup[8] = {'0', '1', '2', '3', '4', '5', '6', '7'};
    int index = tdi + (tms << 1) + (tck << 2);
    return lookup[index];
}

// map from remote-bitbang symbol to TAP input signals
struct signals Jtag::rb_to_signals(char rb_symbol) {
    struct signals sample = {255, 255, 255}; // error value
    switch (rb_symbol) {
        case '0':
            sample = (struct signals){ .tck = 0, .tms = 0, .tdi = 0 };
            break;
        case '1':
            sample = (struct signals){ .tck = 0, .tms = 0, .tdi = 1 };
            break;
        case '2':
            sample = (struct signals){ .tck = 0, .tms = 1, .tdi = 0 };
            break;
        case '3':
            sample = (struct signals){ .tck = 0, .tms = 1, .tdi = 1 };
            break;
        case '4':
            sample = (struct signals){ .tck = 1, .tms = 0, .tdi = 0 };
            break;
        case '5':
            sample = (struct signals){ .tck = 1, .tms = 0, .tdi = 1 };
            break;
        case '6':
            sample = (struct signals){ .tck = 1, .tms = 1, .tdi = 0 };
            break;
        case '7':
            sample = (struct signals){ .tck = 1, .tms = 1, .tdi = 1 };
            break;
        default:
            break;
    }
    return sample;
}

/** Miscellaneous helpers **/

// Compute the required number of rows in the output array
unsigned int Jtag::count_jtag_steps(int n_bytes, struct jtag_programmer_config config) {
    const int steps_jtag_cycle = 2;                                     // timesteps per TCK cycle
    int cycles_shift_addr = config.len_ir + config.len_addr + 11;       // len_ir + len_addr + 11
    int cycles_shift_data = config.len_ir + config.len_data + 11;       // len_ir + len_data + 11
    int cycles_strobe_wen = (config.len_ir + config.len_wen+11) * 2;    // len_ir + len_wen + 11 (twice)
    return n_bytes*steps_jtag_cycle*(cycles_shift_addr+cycles_shift_data+cycles_strobe_wen);
}

void Jtag::print() {
    std::cout << m_waveform << std::endl;
}
