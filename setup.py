from setuptools import setup, Extension
import glob
import numpy

def main():
    cppsources = glob.glob("src/*.cpp")

    setup(
        name = "jtag_kernel",
        version = "0.3.3",
        description = "Compute kernel for generating A-Core JTAG programming waveforms.",
        author = "Verneri Hirvonen",
        author_email = "verneri.hirvonen@aalto.fi",
        ext_modules = [
            Extension(
                "jtag_kernel",
                sources = glob.glob('src/*.cpp'),
                extra_compile_args = ["-std=c++11", "-Wall", "-Wextra", "-march=native", "-O2"],
                language='c++',
                include_dirs=[numpy.get_include()],
            )
        ]
    )

if __name__ == "__main__":
    main()
