# JTAG Kernel
This module is used to generate excitation waveforms for controlling JTAG test logic
on a digital circuit.

## Installation
This module can be compiled and installed on your system with:
```shell
$ python3 setup.py install --user
```

After installation, this module can be imported in python just like any other module:

```python
import jtag_kernel
```
