# SPDX-License-Identifier: Apache-2.0

import unittest
import numpy as np
import jtag_kernel as kernel

class TestMethods(unittest.TestCase):

    def test_wrong_dimensions(self):
        prog_data = np.array([[0, 10, 1],[1, 11, 2]], dtype=np.uint32)
        with self.assertRaises(ValueError):
            kernel.gen_prog_waveform(prog_data,4,1,2,3,17,8,1)
            
    def test_wrong_type(self):
        prog_data = np.array([[0, 10],[1, 11]], dtype=np.byte)
        with self.assertRaises(ValueError):
            kernel.gen_prog_waveform(prog_data,4,1,2,3,17,8,1) 

    def test_wrong_dims(self):
        prog_data = np.zeros((1,2,3), dtype=np.uint32)
        with self.assertRaises(ValueError):
            kernel.gen_prog_waveform(prog_data,4,1,2,3,17,8,1)
    
    def test_valid_input(self):
        prog_data = np.array([[0, 61], [1, 62]], dtype=np.uint32)
        wave = kernel.gen_prog_waveform(prog_data,4,1,2,3,17,8,1)
        self.assertTrue(wave.ndim, 2)
        self.assertTrue(wave.shape[1], 4)
        self.assertTrue(wave.dtype, np.ubyte)
        print(wave)

if __name__ == '__main__':
    unittest.main()
